// console.log("check");




/*
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
*/

/* [Exponent Operator and Template Literals]
3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
*/

//insert code here...



let number = 2;
const getCube = number ** 3;

message = `The cube of ${number} is ${getCube}`;
console.log(message)



/* [Array Destructuring and Template Literals]
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.
*/

//insert code here...

const address = ['258', 'Washington Ave NW', 'California', '90011'];
const [houseNumber, streetName, state, zipCode] = address;

message = `I live at ${houseNumber} ${streetName}, ${state} ${zipCode}`;
console.log(message);



/* [Object Destructuring and Template Literals]
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/



const animal = {
	name : "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	lengthFeet: 20,
	lengthInch: 3
}

const {name, type, weight, lengthFeet, lengthInch} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${lengthFeet} ft ${lengthInch} in.`);



/* [forEach() method and Implicit Return(arrow function => )]
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
*/



let arrayNumbers = [1, 2, 3, 4, 5];
arrayNumbers.forEach(numbers => console.log(numbers));




/* [reduce() method and Implicit Return(arrow function => )]
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers from array of numbers of instuction #9.
*/



const reduceNumber = arrayNumbers.reduce((accumulator, current) => accumulator + current);
console.log(reduceNumber);
;


/*  [Class-Based Object Blueprint / JavaScript Classes]
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/


class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}


let dogClass = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(dogClass);




// 14. Create a git repository named S24.